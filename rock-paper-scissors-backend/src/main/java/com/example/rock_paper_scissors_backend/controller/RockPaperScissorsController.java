package com.example.rock_paper_scissors_backend.controller;

import com.example.rock_paper_scissors_backend.domain.ComputersAnswer;
import com.example.rock_paper_scissors_backend.domain.GameObject;
import com.example.rock_paper_scissors_backend.service.RockPaperScissorsService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@CrossOrigin("origins = *")
public class RockPaperScissorsController {

    private final RockPaperScissorsService rockPaperScissorsService;

    public RockPaperScissorsController(RockPaperScissorsService rockPaperScissorsService) {
        this.rockPaperScissorsService = rockPaperScissorsService;
    }

    @PostMapping("/")
    public ResponseEntity<ComputersAnswer> check(@RequestBody String playersObject) {
        return ResponseEntity.ok(rockPaperScissorsService.check(GameObject.valueOf(playersObject)));
    }

    @GetMapping("/")
    public String check1() {
        return "hello";
    }

}
