package com.example.rock_paper_scissors_backend.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Random;

/**
 * Class responsible for generating random enums
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RandomEnumValue {

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        if (!clazz.isEnum()) {
            throw new IllegalArgumentException("Given class must be an enum type");
        }
        var position = new Random().nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[position];
    }

}
