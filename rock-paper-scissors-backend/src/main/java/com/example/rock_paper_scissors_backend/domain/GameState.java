package com.example.rock_paper_scissors_backend.domain;

public enum GameState {
   PLAYER_WON, PLAYER_LOSE, DRAW
}
