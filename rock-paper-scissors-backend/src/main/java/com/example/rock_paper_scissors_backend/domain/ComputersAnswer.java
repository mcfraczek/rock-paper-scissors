package com.example.rock_paper_scissors_backend.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ComputersAnswer {
    private GameObject gameObject;
    private GameState gameState;
}
