package com.example.rock_paper_scissors_backend.domain;

public enum GameObject {
    ROCK, PAPER, SCISSORS
}
