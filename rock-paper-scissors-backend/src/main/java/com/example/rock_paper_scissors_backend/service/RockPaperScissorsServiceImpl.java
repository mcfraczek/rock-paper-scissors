package com.example.rock_paper_scissors_backend.service;

import com.example.rock_paper_scissors_backend.domain.ComputersAnswer;
import com.example.rock_paper_scissors_backend.domain.GameObject;
import com.example.rock_paper_scissors_backend.domain.GameState;
import com.example.rock_paper_scissors_backend.utils.RandomEnumValue;
import org.springframework.stereotype.Service;

import static com.example.rock_paper_scissors_backend.domain.GameObject.*;
import static com.example.rock_paper_scissors_backend.domain.GameState.*;

@Service
public class RockPaperScissorsServiceImpl implements RockPaperScissorsService {

    @Override
    public ComputersAnswer check(GameObject playersObject) {
        var computersObject = RandomEnumValue.randomEnum(GameObject.class);
        var gameState = check(playersObject, computersObject);

        return ComputersAnswer.builder()
                .gameObject(computersObject)
                .gameState(gameState)
                .build();
    }

    private GameState check(GameObject playersObject, GameObject computersObject) {
        switch (playersObject) {
            case ROCK:
                if (computersObject == ROCK) {
                    return DRAW;
                } else if (computersObject == PAPER) {
                    return PLAYER_LOSE;
                } else if (computersObject == SCISSORS) {
                    return PLAYER_WON;
                }
                break;
            case PAPER:
                if (computersObject == ROCK) {
                    return PLAYER_WON;
                } else if (computersObject == PAPER) {
                    return DRAW;
                } else if (computersObject == SCISSORS) {
                    return PLAYER_LOSE;
                }
                break;
            case SCISSORS:
                if (computersObject == ROCK) {
                    return PLAYER_LOSE;
                } else if (computersObject == PAPER) {
                    return PLAYER_WON;
                } else if (computersObject == SCISSORS) {
                    return DRAW;
                }
                break;
            default:
                throw new IllegalArgumentException("No enum of type" + playersObject);
        }

        throw new IllegalStateException("Method called in an inappropriate way");
    }

}
