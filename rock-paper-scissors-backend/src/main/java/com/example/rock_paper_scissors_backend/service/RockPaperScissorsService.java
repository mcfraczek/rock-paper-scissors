package com.example.rock_paper_scissors_backend.service;

import com.example.rock_paper_scissors_backend.domain.ComputersAnswer;
import com.example.rock_paper_scissors_backend.domain.GameObject;
import com.example.rock_paper_scissors_backend.domain.GameState;

public interface RockPaperScissorsService {
    ComputersAnswer check(GameObject playersObject);
}
