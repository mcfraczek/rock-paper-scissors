export enum GameState {
  PLAYER_WON= 'PLAYER_WON',
  PLAYER_LOSE = 'PLAYER_LOSE',
  DRAW = 'DRAW',
  NEW_GAME = 'NEW_GAME'
}
