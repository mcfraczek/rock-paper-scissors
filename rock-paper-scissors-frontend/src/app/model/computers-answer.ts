import {GameObject} from './game-object';
import {GameState} from './game-state';

export interface ComputersAnswer {
  gameObject: GameObject;
  gameState: GameState;
}
