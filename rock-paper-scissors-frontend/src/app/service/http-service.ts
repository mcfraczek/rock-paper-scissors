import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {GameObject} from '../model/game-object';
import {ComputersAnswer} from '../model/computers-answer';
import {GameState} from '../model/game-state';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  private server = 'http://localhost:8080/';
  subject: BehaviorSubject<ComputersAnswer> = new BehaviorSubject({
    gameObject: GameObject.PAPER,
    gameState: GameState.PLAYER_LOSE
  });

  constructor(
    private readonly http: HttpClient
  ) {
  }

  public check(gameObject: GameObject): Observable<ComputersAnswer> {
    return this.subject.pipe(
      catchError(error => {
        console.error('Error while fetching data', error)
        return throwError(error);
      })
    );
    // return this.http.post(this.server, gameObject) as Observable<ComputersAnswer>;
  }

}
