import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {GameObject} from '../model/game-object';
import {ComputersAnswer} from '../model/computers-answer';
import {randomEnumValue} from '../utils/random-enum-value';
import {GameState} from '../model/game-state';

@Injectable({
  providedIn: 'root',
})
export class GameFrontEndService {

  public check(playersObject: GameObject): Observable<ComputersAnswer> {
    const computersObject: GameObject = randomEnumValue(GameObject);
    const gameState: GameState = this.checkState(playersObject, computersObject);
    const computersAnswer$: BehaviorSubject<ComputersAnswer> = new BehaviorSubject({
      gameObject: computersObject,
      gameState
    });

    return computersAnswer$;
  }

  private checkState(playersObject: GameObject, computersObject: GameObject): GameState {
    switch (playersObject) {
      case GameObject.ROCK:
        if (computersObject === GameObject.ROCK) {
          return GameState.DRAW;
        } else if (computersObject === GameObject.PAPER) {
          return GameState.PLAYER_LOSE;
        } else if (computersObject ===  GameObject.SCISSORS) {
          return GameState.PLAYER_WON;
        }
        break;
      case GameObject.PAPER:
        if (computersObject === GameObject.ROCK) {
          return GameState.PLAYER_WON;
        } else if (computersObject === GameObject.PAPER) {
          return GameState.DRAW;
        } else if (computersObject ===  GameObject.SCISSORS) {
          return GameState.PLAYER_LOSE;
        }
        break;
      case  GameObject.SCISSORS:
        if (computersObject === GameObject.ROCK) {
          return GameState.PLAYER_LOSE;
        } else if (computersObject === GameObject.PAPER) {
          return GameState.PLAYER_WON;
        } else if (computersObject ===  GameObject.SCISSORS) {
          return GameState.DRAW;
        }
        break;
      default:

    }

  }

}
