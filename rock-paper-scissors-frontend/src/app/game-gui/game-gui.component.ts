import {Component} from '@angular/core';
import {faHandPaper, faHandRock, faHandScissors} from '@fortawesome/free-solid-svg-icons';
import {IconDefinition} from '@fortawesome/fontawesome-common-types';
import {SizeProp} from '@fortawesome/fontawesome-svg-core';
import {GameObject} from '../model/game-object';
// import {GameService} from '../service/http-service';
import {ComputersAnswer} from '../model/computers-answer';
import {GameFrontEndService} from '../service/game-front-end-service';
import {GameState} from '../model/game-state';

@Component({
  selector: 'app-game-gui',
  templateUrl: './game-gui.component.html',
  styleUrls: ['./game-gui.component.scss']
})
export class GameGuiComponent {
  public readonly rock: IconDefinition = faHandRock;
  public readonly paper: IconDefinition = faHandPaper;
  public readonly scissors: IconDefinition = faHandScissors;

  public selected: GameObject = undefined;
  public computerSelected: GameObject = undefined;

  public gameState: GameState = GameState.NEW_GAME;

  public readonly size: SizeProp = '10x';

  // there are two implementations - front-end and backend
  constructor(
    // private readonly gameService: GameService,
    private readonly gameService: GameFrontEndService,
  ) {
  }

  public onUnset(): void {
    this.selected = undefined;
    this.computerSelected = undefined;
    this.gameState = GameState.NEW_GAME;
  }

  public onRockClick(): void {
    this.selected = GameObject.ROCK;
    this.gameService.check(GameObject.ROCK).subscribe(
      (value: ComputersAnswer) => {
        this.handleAnswer(value);
      });
  }

  public onPaperClick(): void {
    this.selected = GameObject.PAPER;
    this.gameService.check(GameObject.PAPER).subscribe(
      (value: ComputersAnswer) => {
        this.handleAnswer(value);
      });
  }

  public onScissorsClick(): void {
    this.selected = GameObject.SCISSORS;
    this.gameService.check(GameObject.SCISSORS).subscribe(
      (value: ComputersAnswer) => {
        this.handleAnswer(value);
      });
  }

  private handleAnswer(value: ComputersAnswer): void {
    const gameState = value.gameState;
    const gameObject = value.gameObject;
    if (gameState && gameObject) {
      this.computerSelected = gameObject;
      this.gameState = gameState;
    }
  }
}
