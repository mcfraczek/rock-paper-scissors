import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameGuiComponent } from './game-gui.component';

describe('GameGuiComponent', () => {
  let component: GameGuiComponent;
  let fixture: ComponentFixture<GameGuiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameGuiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameGuiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
